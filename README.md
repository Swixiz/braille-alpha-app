<h1>A propos</h1>
Cette application permet la communication avec l'imprimante Braille Alpha. Elle interragit par Bluetooth avec un module Arduino de l'imprimante. Elle permet d'envoyer le texte qui sera traduit et traité par l'imprimante. De plus, des informations complémentaires sur le projet y sont renseignées. 

<h1>Installation</h1>
L'application est pour l'instant fermée au public et uniquement disponible sur des appareils de test dans le cadre du projet.
L'installation se fait en téléchargeant l'APK sur demande à un administrateur du projet.

<h1>Utilisation</h1>
Pour utiliser l'application, vous devez avoir activé le Bluetooth sur votre téléphone. 
Saisissez le texte à traduire dans le champ correspondant et appuyer sur le bouton traduire. 
Que l'envoi soit réussi ou non, un message apparaitra pour notifier son acheminement.
