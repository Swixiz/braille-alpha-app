import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class TranslateForm extends StatefulWidget {

  @override
  _TranslateFormState createState() => _TranslateFormState();
}

class _TranslateFormState extends State<TranslateForm> {

  final _formKey = GlobalKey<FormState>();
  final translateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Form(
        key: _formKey,
        child: ListView(
          children: [
            const Text("Texte à traduire en braille", style: TextStyle(fontSize: 16, color: Color(0xffa68624)),),
            const SizedBox(height: 5.0,),
            TextFormField(
              maxLines: 17,
              controller: translateController,
              validator: (value)=> value!.isEmpty ? "Précise le texte à imprimer" :null,
              decoration: const InputDecoration(
                  hintText: "Ecrire ici...",
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffa68624), width: 2.0)),
                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffa68624), width: 2.0)),
                )
              ),
            const SizedBox(height: 15.0,),
            FlatButton(
                onPressed: () async {
                  if(_formKey.currentState!.validate()){
                    try {
                      BluetoothConnection connection = await BluetoothConnection.toAddress("20:16:11:07:99:78");
                      List<int> list = ascii.encode(translateController.value.text);
                      Uint8List data = Uint8List.fromList(list);
                      showDialog(context: context, builder: (builder) => CupertinoAlertDialog(
                        title: Text('Debug'),
                        content: Text(data.toString()),
                        actions: [
                          CupertinoDialogAction(child: Text("Ok"), onPressed: () => Navigator.of(context).pop(context)),
                        ],
                      ));
                      connection.output.add(data);
                      connection.finish();
                    }
                    catch (exception) {
                      showDialog(context: context, builder: (builder) => CupertinoAlertDialog(
                        title: Text('Erreur'),
                        content: Text(exception.toString()),
                        actions: [
                          CupertinoDialogAction(child: Text("Ok"), onPressed: () => Navigator.of(context).pop(context)),
                        ],
                      ));

                      print('Cannot connect, exception occured');
                    }
                  }
                },
                child: const Text("Traduire", style: TextStyle(color: Colors.white, fontSize: 16),),
                color: const Color(0xffa68624),
            )
          ],
        ),
      ),
    );
  }
}
