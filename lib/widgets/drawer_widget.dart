import 'package:braille_app/views/about_view.dart';
import 'package:braille_app/views/home_view.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 50.0, horizontal: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Text("Braille Alpha", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.white),),
          const Text("⠃⠗⠁⠊⠇⠇⠑⠀⠁⠇⠏⠓⠁", style: TextStyle(color: Color(0xffa68624), fontSize: 20, fontWeight: FontWeight.bold),),
          const SizedBox(height: 30,),
          ListTile(
            title: const Text("Accueil", style: TextStyle(fontSize: 16, color: Colors.white),),
            onTap: ()=>{
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=>HomeView()))
            },
            leading: const Icon(
              Icons.home,
              color: Colors.white,
            ),
          ),
          ListTile(
            title: const Text("A propos", style: TextStyle(fontSize: 16, color: Colors.white),),
            onTap: ()=>{
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=>AboutView()))
            },
            leading: const Icon(
              Icons.info,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
