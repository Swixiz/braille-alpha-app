import 'package:flutter/material.dart';

class HeaderWidget extends StatelessWidget implements PreferredSizeWidget{

  Size get preferredSize => new Size.fromHeight(60);

  String title;

  HeaderWidget(this.title);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Color(0xffa68624),
      title: Text(this.title),
    );
  }
}
