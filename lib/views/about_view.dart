import 'package:braille_app/widgets/drawer_widget.dart';
import 'package:braille_app/widgets/header_widget.dart';
import 'package:flutter/material.dart';

class AboutView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff4b4b4b),
      appBar: HeaderWidget("A propos"),
      drawer: const Drawer(
        backgroundColor: Color(0xff866d3c),
        child: CustomDrawer(),
      ),
      body: Container(
        margin: const EdgeInsets.all(20.0),
        child: ListView(
          children: const [
            Text("Le projet", style: TextStyle(color: Color(0xffa68624), fontWeight: FontWeight.bold, fontSize: 20)),
            SizedBox(height: 10.0,),
            Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam egestas, sapien eget congue eleifend, ligula nulla vehicula lectus, et ultricies sapien enim vitae turpis. Quisque rhoncus purus vel risus dignissim, at fermentum ligula accumsan. Aenean non ultrices tellus, non viverra ipsum. Suspendisse sit amet erat libero. Nunc semper ultricies libero, a aliquam libero eleifend eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Cras sagittis purus laoreet, tempor eros sit amet, pharetra neque. Vivamus dictum consequat tortor, et convallis eros tincidunt in. Fusce massa purus, efficitur vel euismod at, pharetra sit amet neque. Maecenas eget lectus ac lacus dictum viverra ut quis orci. In efficitur eleifend enim, sit amet venenatis justo blandit nec. Donec sed elit eros. Nullam auctor ipsum sed odio fermentum vestibulum. Vestibulum at orci posuere, rutrum arcu vitae, imperdiet magna. Cras velit lectus, finibus imperdiet purus nec, ultrices ornare ante. Integer quis tempus nulla, placerat lacinia ante. Donec non nisl at enim vulputate semper. Morbi ut dolor sem. Sed ultricies finibus nunc sit amet maximus. Donec at iaculis velit. In hendrerit ex elementum, euismod quam eget, pulvinar est. Morbi venenatis eleifend lorem, id cursus est malesuada ac. Sed ut est vel nisi feugiat porttitor. Mauris quis purus lectus. Curabitur et magna purus. Sed facilisis, risus ut scelerisque varius, purus tortor pharetra nunc, lacinia aliquam arcu leo quis nisi. Mauris quis gravida purus, vel feugiat nisi. Phasellus euismod dui egestas scelerisque maximus. Mauris ac arcu mi. Etiam laoreet vestibulum dapibus. Sed facilisis felis mattis bibendum iaculis.Suspendisse lacinia porttitor nunc lobortis tincidunt. Phasellus varius quam feugiat tincidunt malesuada. Nunc pulvinar, ex sed bibendum accumsan, metus erat varius leo, quis ultrices velit eros in felis. Quisque imperdiet egestas mattis. Morbi nec nulla turpis. Phasellus a ex in ex efficitur hendrerit. Suspendisse eget justo at nisl efficitur ullamcorper sed et nisi. Duis erat augue, laoreet a elit ut, imperdiet fermentum lorem. Nunc a mauris laoreet, varius nunc id, placerat nisi. Sed vitae leo quis orci varius rhoncus non eu dolor. Vivamus sed blandit lorem. Donec tempus eros a leo consectetur tincidunt. Duis massa ante, sagittis a congue id, lacinia in turpis. Quisque aliquet ligula erat, id condimentum neque porta et.Nam mattis lobortis tortor a vulputate. Aliquam venenatis lectus sit amet est dignissim tempus. Curabitur ullamcorper lectus et eros faucibus, vulputate posuere sapien bibendum. Sed hendrerit bibendum eros, ac rutrum quam lobortis quis. Nunc sodales viverra facilisis. Nunc tempor aliquam metus, a porta sem elementum et. Duis ut massa pellentesque elit interdum consequat quis et velit. Suspendisse lacinia tempor dolor id gravida. Morbi at lectus lorem. Aliquam fermentum tempor aliquam. Sed consectetur diam eu leo congue consectetur. Donec a lacinia ex.",
            style: TextStyle(color: Colors.white),),
            SizedBox(height: 30.0,),
            Text("Notre groupe", style: TextStyle(color: Color(0xffa68624), fontWeight: FontWeight.bold, fontSize: 20)),
            SizedBox(height: 10.0,),
            Text("Informations à propos du groupe...", style: TextStyle(color: Colors.white),)
          ],
        ),
      ),
    );
  }
}
