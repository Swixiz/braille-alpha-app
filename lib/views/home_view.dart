import 'package:braille_app/widgets/drawer_widget.dart';
import 'package:braille_app/widgets/header_widget.dart';
import 'package:braille_app/widgets/translate_form_widget.dart';
import 'package:flutter/material.dart';

class HomeView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffa68624),
        onPressed: ()=>{
          // TODO
        },
        child: Icon(Icons.photo_camera_outlined, color: Colors.white),
      ),
      backgroundColor: Color(0xff4b4b4b),
      appBar: HeaderWidget("Accueil"),
      drawer: Drawer(
        backgroundColor: Color(0xff866d3c),
        child: CustomDrawer(),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(vertical:5.0, horizontal: 20.0),
        child: TranslateForm(),
      ),
    );
  }
}
